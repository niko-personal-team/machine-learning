function filtdata = hpfilter(data, sampling, hpcutoff)

% filtdata = hpfilter(data, sampling, hpcutoff);
%
%
%Description:
%
%This function highpass filters the input signal at the specified cutoff
%frequency.  
%
%Parameters:
%
%data       -  matrix containing data where each COLUMN corresponds to a channel
%sampling   -  the sampling rate that was used in collecting the data
%hpcutoff   -  the cutoff frequency desired for the highpass filter
%
%Delsey Sherrill 1/18/03
%updated 4/21/05:  Improve memory usage by not manipulating input data so it can handle larger data
% sets.  Also removed "detrend" before filtering b/c it serves no purpose.

% transpose data if channels are in the ROWS
if size(data,2) > size(data,1)
    warning('Transposing data because it appears that channels are in the ROWS')
    data = data';
end

%design highpass filter to attenuate motion artifacts
tw = 0.1;  % width of transition band, Hz
%tw = 5;  % width of transition band, Hz
Rp = 0.5;  % passband tolerance, dB 
%Rs = 40;   % minimum attenuation in stopband, dB 
Rs = 40;   % minimum attenuation in stopband, dB 
wp = (hpcutoff+tw/2)/(sampling/2);
ws = (hpcutoff-tw/2)/(sampling/2);

% [N, Wn] = buttord(wp, ws, Rp, Rs);
% N=6; Wn = hpcutoff/(sampling/2);
% [b,a] = butter(N,Wn,'high');

[N, Wn] = ellipord(wp,ws,Rp,Rs);
[b,a] = ellip(N,Rp,Rs,Wn,'high');

% %show a plot of filter
% figure,freqz(b,a),subplot(211),title('plot of hpfilter')  

%implement filter in non-causal manner
filtdata = filtfilt(b,a,data);

