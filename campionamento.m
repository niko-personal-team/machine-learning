% ordine colonne y z x

speed_f = hpfilter(speed((5:3005),[3 2 1]),50,0.5);
wall_f = hpfilter(wall((5:3005),[3 2 1]),50,0.5);
fermo_f = hpfilter(fermo((5:3005),[3 2 1]),50,0.5);

speed_m = sqrt(speed_f(:,1).^2+speed_f(:,2).^2+speed_f(:,3).^2);
wall_m = sqrt(wall_f(:,1).^2+wall_f(:,2).^2+wall_f(:,3).^2);
fermo_m = sqrt(fermo_f(:,1).^2+fermo_f(:,2).^2+fermo_f(:,3).^2);

speed_features = zeros(0,5);
win_dim = 50;
start_idx = 1;
end_idx = start_idx + win_dim-1;

while(end_idx <=length(speed_m))
    window = speed_m(start_idx:end_idx);
    
    features =zeros(1,5);
    
    features(1,1) = mean(window);
    features(1,2) = std(window);
    features(1,3) = range(window);
    
    dm = window(2:end) - window(1:(end-1));
    dt = 20; %delta time 20ms
    
    features(1,4) = mean(dm./dt);
    
    features(1,5) = 1;
    
    speed_features = vertcat(speed_features,features);
    
    start_idx = start_idx + win_dim;
    end_idx = start_idx + win_dim-1;
    
end

wall_features = zeros(0,5);
start_idx = 1;
end_idx = start_idx + win_dim-1;

while(end_idx <=length(wall_m))
    window = wall_m(start_idx:end_idx);
    
    features =zeros(1,5);
    
    features(1,1) = mean(window);
    features(1,2) = std(window);
    features(1,3) = range(window);
    
    dm = window(2:end) - window(1:(end-1));
    dt = 20; %delta time 20ms
    
    features(1,4) = mean(dm./dt);
    features(1,5) = 2;
    wall_features = vertcat(wall_features,features);
    
    start_idx = start_idx + win_dim;
    end_idx = start_idx + win_dim-1;
    
end

fermo_features = zeros(0,5);
start_idx = 1;
end_idx = start_idx + win_dim-1;

while(end_idx <=length(fermo_m))
    window = fermo_m(start_idx:end_idx);
    
    features =zeros(1,5);
    
    features(1,1) = mean(window);
    features(1,2) = std(window);
    features(1,3) = range(window);
    
    dm = window(2:end) - window(1:(end-1));
    dt = 20; %delta time 20ms
    
    features(1,4) = mean(dm./dt);
    features(1,5) = 3;
    fermo_features = vertcat(fermo_features,features);
    
    start_idx = start_idx + win_dim;
    end_idx = start_idx + win_dim-1;
    
end



wall_c = 1;
speed_c = 2;
fermo_c = 3;


dataset = vertcat(wall_features,speed_features,fermo_features);

[train_idx val_idx test_idx] = dividerand(length(dataset),0.7,0.15,0.15);
training_set = dataset(train_idx,:);
testing_set = dataset(test_idx,:);

t = fitctree(training_set(:,1:(end-1)), training_set(:,end)); % features,labels

view(t);
view(t,'mode','graph');

prediction = predict(t,testing_set(:,1:(end-1)));



out = prediction - testing_set(:,5);

accuracy =  sum(out==0)/length(out);

